# MSIMysticLightDatabase

Database of MSI Mystic Light firmware dumps

# Board Lookup Table

| USB PID | Protocol | Board Name                      |
| ------- | -------- | ------------------------------- |
| 7B85    | 162-byte | MSI B450 GAMING PRO Carbon WiFi |
| 7C35    | 185-byte | MSI X570 ACE                    |
| 7C56    | 185-byte | MSI B550 MPG Gaming Plus        |
| 7C83    | 185-byte | MSI B460M PRO-VDH WiFi          |